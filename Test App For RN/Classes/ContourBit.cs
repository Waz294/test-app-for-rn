﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Test_App_For_RN.Interfaces;

namespace Test_App_For_RN.Classes
{
    class ContourBit : IContourBit, IContourBitEdit, IEnumerable<Point>
    {
        List<Point> contourPoints;

        public ContourBit()
        {
            contourPoints = new List<Point>();

            IsClosed = false;
        }

        public bool IsClosed { get; set; }

        public int GetCount { get { return contourPoints.Count; } }

        public void AddPoint(Point point)
        {
            contourPoints.Add(point);
        }

        public IEnumerator<Point> GetEnumerator()
        {
            if (IsClosed)
            {
                return new ClosedEnumerator(this);
            }
            else
            {
                return contourPoints.GetEnumerator();
            }
        }

        public Point GetPoint(int index)
        {
            return contourPoints[index];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void RemovePointAtIndex(int index)
        {
            if (index > 0 || index < GetCount)
            {
                contourPoints.RemoveAt(index);
            }
        }

        private class ClosedEnumerator : IEnumerator<Point>
        {
            int position = -1;
            bool End = false;
            IContourBit bit;

            public Point Current
            {
                get
                {
                    try
                    {
                        return bit.GetPoint(position);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }

            public ClosedEnumerator(IContourBit bit)
            {
                this.bit = bit;
            }

            public void Dispose()
            { }

            public bool MoveNext()
            {
                if (End)
                {
                    return false;
                }

                position++;
                var res = (position < bit.GetCount);

                if (!res && !End)
                {
                    End = true;
                    position = 0;
                    return true;
                }

                return res;
            }

            public void Reset()
            {
                position = -1;
                End = false;
            }
        }
    }
}