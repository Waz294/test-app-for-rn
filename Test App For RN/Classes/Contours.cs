﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test_App_For_RN.Interfaces;

namespace Test_App_For_RN.Classes
{
    class Contours : IContours, IContoursEdit
    {
        List<IContour> contours;

        public Contours()
        {
            contours = new List<IContour>();
        }

        public int GetCount { get { return contours.Count; } }

        public void AddContour(IContour contour)
        {
            contours.Add(contour);
        }

        public IContour GetContour(int index)
        {
            if (index < 0 || index > GetCount - 1)
            {
                return null;
            }

            return contours[index];
        }

        public IEnumerator<IContour> GetEnumerator()
        {
            return contours.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}