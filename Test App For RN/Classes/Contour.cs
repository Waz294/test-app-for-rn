﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test_App_For_RN.Interfaces;

namespace Test_App_For_RN.Classes
{
    class Contour : IContour, IContourEdit
    {
        List<IContourBit> contourBits;

        public Contour()
        {
            contourBits = new List<IContourBit>();

            IsSelected = false;
        }

        public int GetCount { get { return contourBits.Count; } }

        public bool IsSelected { get; set; }

        public void AddContourBit(IContourBit contourBit)
        {
            contourBits.Add(contourBit);
        }

        public IContourBit GetContourBit(int index)
        {
            if (index < 0 || index > GetCount - 1)
            {
                return null;
            }

            return contourBits[index];
        }

        public IEnumerator<IContourBit> GetEnumerator()
        {
            return contourBits.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
