﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_App_For_RN.Interfaces
{
    interface IContours : IEnumerable<IContour>
    {
        int GetCount { get; }

        IContour GetContour(int index);
    }
}
