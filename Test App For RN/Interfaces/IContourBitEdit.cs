﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Test_App_For_RN.Interfaces
{
    interface IContourBitEdit : IContourBit
    {
        void AddPoint(Point point);

        new bool IsClosed { get; set; }
    }
}
