﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Test_App_For_RN.Interfaces
{
    interface IContourBit : IEnumerable<Point>
    {
        bool IsClosed { get; }
        


        int GetCount { get; }

        Point GetPoint(int index);
    }
}
