﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_App_For_RN.Interfaces
{
    interface IContoursEdit : IContours
    {
        void AddContour(IContour contour);
    }
}
