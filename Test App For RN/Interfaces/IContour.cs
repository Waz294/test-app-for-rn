﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_App_For_RN.Interfaces
{
    interface IContour : IEnumerable<IContourBit>
    {
        int GetCount { get; }

        bool IsSelected { get; }

        IContourBit GetContourBit(int index);
    }
}
