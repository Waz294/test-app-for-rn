﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using Test_App_For_RN.Interfaces;
using Test_App_For_RN.Classes;
using System.Windows.Threading;

namespace Test_App_For_RN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Rectangle rectangle; //For drawing
        private Rect rectSelected; //For calculations
        private Point start;
        private IContours contours;
        private IContour selectedContour;

        public MainWindow()
        {
            InitializeComponent();

            ImportContours();

            DrawContours(contours);
        }

        private void ImportContours()
        {
            var arr = JArray.Parse(File.ReadAllText("../../Input/Contours.json"));

            IContoursEdit contoursEdit = new Contours();

            contours = contoursEdit;

            foreach(var child1 in arr)
            {
                IContourEdit contour = new Contour();
                
                foreach(var child2 in child1.Value<JArray>("ContourBits"))
                {
                    IContourBitEdit contourBit = new ContourBit();

                    foreach(var child3 in child2.Value<JArray>("Points"))
                    {
                        Point point = new Point
                        {
                            X = child3.Value<double>("X"),
                            Y = child3.Value<double>("Y")
                        };

                        contourBit.AddPoint(point);
                    }

                    contourBit.IsClosed = child2.Value<bool>("IsClosed");

                    contour.AddContourBit(contourBit);
                }

                contoursEdit.AddContour(contour);
            }
        }

        private void DrawContours(IContours contours)
        {
            foreach(var cnt in contours)
            {
                DrawContour(cnt);
            }
        }

        private void DrawContour(IContour contour)
        {
            foreach (var bit in contour)
            {
                var poly = new Polyline();
                if (contour.IsSelected)
                {
                    poly.Stroke = Brushes.Red;
                }
                else
                {
                    poly.Stroke = Brushes.Black;
                }

                poly.StrokeThickness = 2;
                poly.Points = new PointCollection(bit);
                frontCanvas.Children.Add(poly);
            }
        }

        private void ClearSelection()
        {
            if (selectedContour != null)
            {
                frontCanvas.Children.RemoveRange(frontCanvas.Children.Count - selectedContour.GetCount - 1, selectedContour.GetCount + 1);
                selectedContour = null;
            }
        }
    
        private void FrontCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            start = e.GetPosition(frontCanvas);

            if (rectangle != null)
                frontCanvas.Children.Remove(rectangle);

            rectangle = new Rectangle
            {
                Stroke = Brushes.LightBlue,
                StrokeThickness = 2
            };

            rectSelected = new Rect(start, start);

            Canvas.SetLeft(rectangle, start.X);
            Canvas.SetTop(rectangle, start.Y);
            frontCanvas.Children.Add(rectangle);
        }

        private void FrontCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released || rectangle == null)
                return;

            var currPos = e.GetPosition(frontCanvas);

            rectangle.Stroke = new SolidColorBrush(Colors.Black);

            var x = Math.Min(currPos.X, start.X);
            var y = Math.Min(currPos.Y, start.Y);

            Canvas.SetLeft(rectangle, Math.Min(currPos.X, start.X));
            Canvas.SetTop(rectangle, Math.Min(currPos.Y, start.Y));

            rectangle.Width = Math.Abs(currPos.X - start.X);
            rectangle.Height = Math.Abs(currPos.Y - start.Y);

            rectSelected.Location = new Point(x, y);
            rectSelected.Width = rectangle.Width;
            rectSelected.Height = rectangle.Height;
        }

        private void CutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();

            selectedContour = CutContourByWindow(contours, rectSelected);

            if (selectedContour != null)
            {
                DrawContour(selectedContour);
            }
        }

        private IContour CutContourByWindow(IContours contours, Rect window)
        {
            if(contours.GetCount == 0)
                return null;
           
            Func<Point, Rect, bool> FindPointInRect = (point, rect) =>
            {   
                // Should use rect.Contains(point)
                return point.X > rect.Location.X && point.X < rect.BottomRight.X && point.Y > rect.Location.Y && point.Y < rect.BottomRight.Y;
            };

            Func<Point, Point, Point, Point, bool> CheckIntersection = (p1, p2, p3, p4) => 
            {
                var v1 = (p4.X - p3.X) * (p1.Y - p3.Y) - (p4.Y - p3.Y) * (p1.X - p3.X);
                var v2 = (p4.X - p3.X) * (p2.Y - p3.Y) - (p4.Y - p3.Y) * (p2.X - p3.X);
                var v3 = (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
                var v4 = (p2.X - p1.X) * (p4.Y - p1.Y) - (p2.Y - p1.Y) * (p4.X - p1.X);
                return (v1 * v2 <= 0) && (v3 * v4 <= 0);
            };

            Func<Point, Point, Rect, bool> CheckIntersectLineAndWindow = (p1, p2, rect) =>
            {
                return CheckIntersection(p1, p2, rect.TopLeft, rect.BottomLeft) ||
                      CheckIntersection(p1, p2, rect.BottomLeft, rect.BottomRight) ||
                      CheckIntersection(p1, p2, rect.BottomRight, rect.TopRight) ||
                      CheckIntersection(p1, p2, rect.TopRight, rect.TopLeft) ||
                      FindPointInRect(p1, rect) || FindPointInRect(p2, rect);
            };

            Func<Point, Point, IContourEdit, bool> addNewContourBit = (p1, p2, contour) =>
            {
                IContourBitEdit newBit = new ContourBit();

                newBit.AddPoint(p1);
                newBit.AddPoint(p2);

                contour.AddContourBit(newBit);

                return true;
            };

            IContourEdit cnt = new Contour();

            cnt.IsSelected = true;

            foreach(var contour in contours)
            {
                if (contour == null)
                    continue;

                foreach(var bit in contour)
                {
                    if (bit == null)
                        continue;

                    for(var i = 0; i < bit.GetCount - 1; i++)
                    {
                        Point p1 = bit.GetPoint(i);
                        Point p2 = bit.GetPoint(i + 1);

                        if (CheckIntersectLineAndWindow(p1, p2, window))
                        {
                            addNewContourBit(p1, p2, cnt);
                        }
                    }

                    // Checking last segment of closed contourBit
                    if (bit.IsClosed)
                    {
                        Point p1 = bit.GetPoint(bit.GetCount - 1);
                        Point p2 = bit.GetPoint(0);

                        if (CheckIntersectLineAndWindow(p1, p2, window))
                        {
                            addNewContourBit(p1, p2, cnt);
                        }
                    }
                }
            }

            if (cnt.GetCount > 0)
                return cnt;
            else
                return null;
        }
    }
}